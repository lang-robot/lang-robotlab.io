<div align="center">

# Politechnika Warszawska
## Prototypowanie inżynierskie w automatyce i robotyce

</div>
<div align="right">

### Gabriel Brzeziński
### Stankevich Stanislau

</div>

# Language learning robot
Zadaniem jest opracowanie robota wspomagającego naukę języków 
## Zespół projektowy
- Gabriel Brzeziński
- Stankevich Stanislau
## Początkowe hipotezy
### Większość dzieci w Polsce uczy się języka angielskiego:  
- Robot powinien skupić się na tym języku  

### Użytkownicy oczekują pojawiania się regularnie nowych ćwiczeń:  
- Robot powinien mieć możliwość aktualizacji 

### Dzieci zaczynają naukę języków już w przedszkolach, ale kontynuują ją nawet w liceum:
- Robot powinien mieć długi czas życia (kilka lat). 
- Robot powinien być odpowiedni zarówno dla dziecka w wieku 6-lat oraz dla nastolatka:
    - Wygląd musi być uniwersalny
    - Powinna być możliwość konfiugracji ćwiczeń
        - Konfigurowalna trudność ćwiczeń
        - Konfigurowalny rodzaj ćwiczeń np. nauka poprzez kalambury, nauka poprzez quizy
    - Robot powinien mieć możliwość konfiguracji głosu:
        - Konfiguracja wieku np. głos 6-cio latka lub głos dorosłego nauczyciela
        - Konfiguracja płci (głos męski, głos żeński)

### Robot będzie używany przez małe dzieci:
- Robot musi mieć bezpieczny kształt i wygląd
- Robot powinien działać, nawet jeżeli dziecko nie ma dostępu do gniazdka  
    - Robot powinien działać na akumulatorach/bateriach
        - Robot powinien działać na akumulatorach wystarczająco długo by wykonać wszystkie zaplanowane ćwiczenia (~3h)

### Robot będzie używany przez starsze dzieci. Nastolatkowie spędzają znaczący czas przy biurku (odrabianie lekcji oraz w czasie wolnym):
- Robot powinien bez problemu mieścić się na biurku
- Robot powinien być lekki dla nastolatka

### Rodzice oczekują, że robot samodzielnie będzie uczył dziecko języka (bez nadzoru rodzica):
- Robot powinien samodzielnie inicjować i zachęcać dziecko do nauki
    - Robot powinien skupiać uwagę dziecka na sobie np. przez:
        - System audio
        - Wizualnie:
           - Za pomocą ekranu
           - Za pomocą ruchu i gestykulacji
- Możliwość konfiguracji ilości ćwiczeń w ciągu dnia/tygodnia
- Możliwość sprawdzenia postępu w nauce dziecka

### Inne:
- Zgodnie z poprzednimi punktami robot powinien być konfigurowalny np. przez aplikację, stronę lub ekran dotykowy
- Użytkownicy będą oczekiwać, że robot nie będzie kosztować więcej niż inne standardowe systemy inteligentne (jak na przykład odkurzacz), więc cena urządzenia powinna być poniżej 1000 - 2000 PLN

<div style="page-break-after: always;"></div>

## Mapa empatii
### Co widzi?
- Młodsze dzieci preferują naukę przez zabawę
- Starsze dzieci preferują naukę przez konwersacje
- Dzieci uczyłyby się głównie angielskiego, ale niemiecki, rosyjski i hiszpański też by się im przydały
- Młodsze dziecko mogłoby się uczyć polskiego 
- Nauka innych rzeczy niż języki byłaby dla dzieci dodatkową zaletą
### Co słyszy?
- Byłoby dobrze by urządzenie integrowało się z innymi systemami inteligentnymi - asystent głosowy, systemy kontroli rodzicielskiej
- Dzieci grają w gry komputerowe i oglądają filmy w Internecie
### Co myśli i czuje?
- Urządzenie takie jest warte 1000-2000 zł
- Urządzenie będzie użytkowane przez wiele lat (3-10)
- Równie ważne co nauka języków jest samo spędzanie czasu z dzieckiem
- Urządzenie powinno mieć własną osobowość (imię, płeć)
### Co mówi i robi?
- Ma dzieci w wieku 1 rok - 15 lat
- Z robota będzie korzystało więcej niż jedno dziecko
- Nie nadzoruje nauki dziecka
### Jakie są jego obawy?
- Dziecko będzie ignorować urządzenie
- Urządzenie nie powinno nagrywać otoczenia
- Urządzenie powinno być bezpieczne dla dziecka
- Urządzenie musi być trudne do uszkodzenia przez dziecko
### Jakie są jego potrzeby?
- Robot będzie raczej pracował stacjonarnie, nie musi samo się przemieszczać
- Urządzenie powinno się aktualizować przez Internet
- Powinna być możliwość śledzenia postępów nauki dziecka (zbieranie statystyk)
- Robot powinien umożliwiać konfigurację głosu, tematów do konwersacji oraz ćwiczeń
- Robot powinien być zarządzany oraz konfigurowany przez ekran dotykowy i aplikację mobilną
- Urządzenie powinno mieścić się na biurku i być możliwe do podniesienia przez 6-cio letnie dziecko (~1kg)
- Dodatkową zaletą będzie, jeżeli robot działa poza domem
- Robot powinien działać ok. 5h na akumulatorach
- Stacja dokująca byłaby wygodniejsza niż ładowanie kablem

<div style="page-break-after: always;"></div>

## Persona - rodzic
![Mężczyna z laptopem](docs/tomek.jpg)

[source]: # (https://c.pxhere.com/images/da/40/184275eb8ea77819fdf79b6d6c87-1434701.jpg!d)

- Imię: Tomek
- Wiek: 37
- Płeć: Mężczyzna
- Zawód: Informatyk
- Zarobki: 16 000
- Status: Żonaty, 1 dziecko (8 lat)
- Motto: "Samo się zrobi"
### Biogram
Pracuje w branży technologicznej, jest mocno zaangażowany. Przywiązuje wysoką wagę do edukacji (własnej i dziecka). Ma żonę, która pracuje w prasie.
### Cele
Chce mieć więcej czasu na rozwój siebie i swoich projektów, ale nie chce zaniedbywać rozwoju własnego dziecka.
### Zachowania
Przez większość dnia pracuje, zdarza mu się zostawać po godzinach. Obiad je na mieście. Kolacje przeważnie przygotowuje żona, która wcześniej wraca z pracy. W wolnym czasie albo ogląda coś na netflixie, albo czyta artykuły z branży. 
### Frustracje
Nie znosi obowiązków domowych. Gdy żona kazała mu regularnie odkurzać, to kupił na drugi dzień roombe.  Uważa, że konwencjonalne metody nauki w szkole, są niewystarczające.

<div style="page-break-after: always;"></div>

## User story
- Rodzic chce, by dziecko podejmowało samodzielnie naukę w czasie wolnym od zajęć
- Rodzic chce, by dziecko mogło się łatwo uczyć w trakcie dłuższych podróży
- Rodzic chce, by robot uczył dziecka wszystkich aspektów języka angielskiego w tym słuch i mowę
- Rodzic chce, by nie była wymagana jego interwencja, kiedy dziecko uczy się z robotem. 
- Rodzic chce mieć możliwość sprawdzenia statystyk z ćwiczeń, by móc zweryfikować postęp w nauce dziecka
- Rodzic chce mieć możliwość konfiguracji robota poprzez swój telefon
- Rodzic chce by robot inicjował naukę i do niej skutecznie zachęcał

## Analiza konkurencji
### **[NAO and Pepper](https://www.softbankrobotics.com/emea/en/nao)**
![Nao robot](docs/nao.jpg)

[source]: # (https://upload.wikimedia.org/wikipedia/commons/4/47/Nao_Robot_%28Robocup_2016%29.jpg)

Nao i pepper to jedne z bardziej zaawansowanych robotów edukacyjnych. I główną cechą jest duże zastosowanie pod względem robotyki - Nao jest robotem humanoidalnym o 25 stopniach swobody, a pepper zamiast nóg ma platformę kołową. Oba roboty są bogato wyposażone w różne sensory, mające jeszcze bardziej rozszerzyć ich możliwości. Roboty są kierowane w wiele obszarów rynku, pod względem edukacji oferują głównie możliwość ich programowania. Dostępne są na nie jednak aplikacje i lekcje oferowane przez inne firmy, które pozwalają na naukę takich przedmiotów jak na przykład matematyka, fizyka, języki obce oraz zdolności pisania. Robot jest dostępny jedynie dla szkół i instytucji naukowych. Nao kosztuje od 8 000$ do 13 000$. Pepper kosztuje 1 500$, jednak sprzedawany jest razem z roczną subskrypcją, co podnosi koszt robota to tej samej ceny co Nao. Pepper jest nowszy od NEO, jednak jego produkcja została wstrzymana, ze względu na niski popyt. 

<div style="page-break-after: always;"></div>

Opinie:
- > We had one at my University. It was neat, for about 10 minutes. It broke often, overheated, was underpowered, the battery didn't last long, and most of the samples didn't work.
    >
    >That being said, I'm not sure if there is anything better.
- >  Had one borrowed from a school, it’s not that super clever, you get bored of it quite fast.
- > It's a hefty price for a robot that doesn't do anything by default. (Other than standing around and giving canned responses), and even then it's not *that* capable. Hand flexors are not that strong, can only grab light objects. It does not have proper depth sensing, tends to overheat when overusing the gears, etc. etc.
    >
    >It's like having a little handicapped guy with you.
    >
    >Only buy if you have more money than you know what to do with and you're some tech savvy programmer that wants to have some fun with expensive gadgets.
- > When I did STEM horizons we got to interact with these. They can actually recognise objects put in front of them, and they wave at you. And the best part - THEY CAN SHAKE YOUR HAND.
    >
    > Regardless, these things are awesome.
- >There's one in bologna airport, Italy it's surprisingly UNhelpful only showing the information desk, and while it has an option to reply in Italian it doesn't seem to understand it.
- >These used to be in SoftBank store locations in Japan, they were expressive but not at all helpful. I used to try to ask it how the weather was or general questions and they could do nothing. The staff there (at the location I would visit) hated them as they didn’t know how to use them and would have no answer as to what they did.

Zalety:
- Rozpoznaje obiekty, które się przed nim znajdują!
- A nawet może podać rękę!

Wady: 
- Jest drogi
- Szybko się nudzi
- Domyślnie nie wiele robi - jest potrzeba samodzielnego oprogramowania robota
- Ma ręce, ale słabe
- Przegrzewa się
- Krótko działa na baterii
- Ma wiele sensorów, ale nie ma faktycznego postrzegania głębi obrazu
- Robot jest mało pomocny, nie radzi sobie z prostymi pytaniami
- Nie radzi sobie z rozumieniem mowy w głośniejszych miejscach (sklepy, lotniska, targi)

<div style="page-break-after: always;"></div>

### **[Elias Robot](https://www.eliasrobot.com/)**
![Ćwiczenia oferowane przez elias robot](docs/elias.png)

[source]: # (https://www.eliasrobot.com/for-teachers)

*Elias Robot* to aplikacja do nauki języków dla robota NAO. Obsługuję naukę angielskiego, fińskiego, szwedzkiego, hiszpańskiego oraz niemieckiego. Aplikacja była przez ponad rok testowana w szkole podstawowej w Finlandii. 

Opinie:
- >I see Elias as one of the tools to get different kinds of practice and different kinds of activities into the classroom
- >During my lessons, I use the robot for getting to know the vocabulary, for instance, for approximately 15 minutes at a time. After that, we move on to other exercises. Pupils have asked me plenty of questions about robotics, and we have had a lot of fun with Elias during our lessons
- >The wide language selection has been a real treat because Tampere is offering earlier foreign language education. Currently, the robot lessons are especially well-suited for preschool and primary education. It’s great that the City of Tampere has wanted to invest in robotics.
- > I think in the new curriculum the main idea is to get the kids involved and get them motivated and make them active. I see Elias as one of the tools to get different kinds of practice and different kinds of activities into the classroom
- > Kids can practice language without fear of making mistakes. Robot is easy to approach. It doesn't laugh If you make mistake and it doesn't get tired of repeating words.
- > Elias brings a happy and safe atmosphere to the classroom. It is a diverse motivator for learning.
- > There was a boy in the class who had not said a single word during previous English lessons. Now it is him who wants to be the first to talk to Elias.

Zalety:
- Nie zniechęca do nauki, nie karci za błędy
- Obsługuje wiele języków
- Posiada wiele różnych rodzajów ćwiczeń
- Wzbudza zainteresowanie w dzieciach
- Wprowadza przyjazną atmosferę dla nauki dzieci

<div style="page-break-after: always;"></div>

### **[Engage-k12](https://engagek12.robotlab.com/)**
![Ćwiczenia oferowane przez robotlabs](docs/engage-k12.png)
Engage-k12 to różne lekcje oferowane przez firmę robotlab dla robotów takich jak NEO, czy Pepper, ale również na inne platformy jak roboty mobilne, manipulatory, drony, okulary VR oraz drukarki 3d. Kursy obejmują materiał m. in. z matematyki, fizyki, programowania oraz ze zdolności językowych.

Opinie:
- > Engage! K12 sets everything up for me. It gave me activities for my students to do with the NAO robot so I could make sure that they master the skills and it gave me the extra fun piece that I was looking for in my classes.
- Opinia na temat zestawu VR
    > I cannot say enough about how amazing these devices are! Our students loved taking field trips to faraway places, and our teachers love the ability they have to lead those trips. Additionally, RobotLab offers excellent tech support. Shout out to Paul for answering all my questions quickly and thoroughly. These devices are pricey, but are worth every cent!
- > The reward of seeing it run on a robot gives them motivation to learn it better and understand it better and everyone comes out knowing the concepts

Zalety: 
- Nie wymaga dodatkowej obsługi czy przygotowania
- Dobre wsparcie techniczne
- Daje motywację do nauki

<div style="page-break-after: always;"></div>

### **[Keeko](http://www.ikeeko.com/)**
![](docs/keeko.jpeg)

Keeko to robot edukacyjny dla mniejszych dzieci (3-6) wypracowany przez informatyków chińskich z naciskiem na sztuczną inteligencję. Jest skierowany do użycia w przedszkolach, podstawówkach, ale również w domu. Ma wysokość 60 cm. Jest mobilny (ma małe koła). Jest on głównie skierowany do nauki informatyki. Jest to produkt chiński i na rynek Chiński. KeeKo korzysta z wielu inteligentnych usług, takich jak rozpoznawanie obrazu, rozpoznawanie twarzy, rozpoznawanie mowy. Istnieje również technologia rozpoznawania dotyku, dzięki 8 punktom dotykowym, która może wyczuć dotyk dziecka i na niego reagować.

Cena: ok. 6600 zł (Dane z 2016 roku)


Opinie:
- Automatycznie przetłumaczone z chińskiego na polski:
    > The first thing is to look at the face. Keeko is a cute robot that looks like a robot cat and a bit like a condensed version of Dabai. You can see that my engineering men are all cute
- Automatycznie przetłumaczone z chińskiego na polski:
    > It is almost equivalent to an intelligent robot that can autonomously learn, communicate and make a series of actions. (I heard that it can be used as a home monitoring device, I haven't tried it with video calls or something)
- > When children see Keeko with its round head and body, it looks adorable and children love it. So when they see Keeko, they almost instantly take to it
- > With cute voice and appearance, the robot is easily accepted by children

Zalety:
- Przyjazny wygląd dla dzieci
- Możliwość szerokiego zastosowania


<div style="page-break-after: always;"></div>

### **[Cozmo](https://www.digitaldreamlabs.com/products/cozmo-robot)**

![](./docs/cozmo.jpg)

Robot z własną osobowością. Ma pełnić rolę zwierzaka, towarzysza. Poza różnymi grami i zabawami, robot umożliwia naukę programowania. Jest wyposażony w kamerę, ekran, koła i stację dokującą.

Cena: ~300 $

Opinie:
- > I am software engineer employed in robotics company, may conclude that the toy itself doesn't represent any breakthrough from engineering perspective. Well done mechanical and electronics design, just a few motors, OLED screen and camera controlled from the phone app. BUT! The amount of game design that have been put into this toy is stunning. Everything is done so well (animatronics, sounds, screen face animations) that it makes you truly believe that it is a living thing. When it comes to things like this toy - good engineer is useless without a good UX designer, as well as great UX design can be spoiled with rookie mechanical, electronics or software implementation. Anki seems got a great teem engineers and designers covering everything perfectly right, and these guys produced an amazing product which is so rare in modern days. Bravo!

- > This one is a mixed bag. It came well packed, and clearly a lot of thought went into development and design, but it did exactly what I was afraid it would do, especially at this price: it got played with for a week, and then never touched again. Every 6 months or so when I float the idea of getting rid of it there will be a day or two of play but that's it. It just... can't... do very much. It's very cute and all, but not very engaging past a few games.

- > First of all, my kids received this little guy for Christmas and we don't know who likes him more, the kids or adults! He truly is so very intelligent and the random things he does are hilarious! We actually feel bad when we beat him in games (although it is so funny to see him get mad). It was so cute to see Cozmo first run into our pets! Beware though, he does not like it when you move his blocks or if you hold him!!! Lol. On another note, I do wish he acted a little more independent from the app, but maybe that will come with time.
- > I received it as a gift 2 years ago I believe, maybe 3. The packaging was clean, and opening it and setting it up was a joy. For the first month it was a fun distraction for about 30 mins a day. It was also fun to break it out and show people that didn’t know what it was. Once you unlock all of it’s abilities it quickly lost its fun factor. Now I bring it out a few times a year to show people or when my kids want to play with it. There does seem to be a small community of people that enjoy the light programming aspect that comes with it, but that’s not really my jam. I think it boils down to a cool desk toy.
    >
    > My biggest gripe is that it won’t auto dock on its charger. Major missed opportunity there.
- > We have major issues with his wifi connectivity to our devices. I've tried a lot of things and it gets quite frustrating how hard it is to keep a device connected to Cozmo.
    >
    > If we could stay connected, I would rate it a 10/10 product - but the connectivity challenges (that from what I see in google searches are not uncommon) I'd give it a 6/10.
    >
    >I work from home, have other kids, and to FULLY turn all other internet off is usually just not an option for my child to play with the toy.
- > I have a bookend Cozmo.....breaks my heart. He was great new, then slowly his play time dropped and I wound up sending him in under warranty and getting another one that has only to work a short time as well. I love Coz, all his antics and expressions and interaction, you really get attached to him. I consider him the best entertaining toy/gift I had ever gotten. But his connection is wonky, his battery usage is wonky, They just need to redesign his mechanical and connection features and keep all his programming and facial expressions intact. If they "fix" that, I will definately buy one. He's a keeper, he needs someone to really rescue him from his shortcomings.
- > The most important thing to remember is that Cozmo only has one cliff sensor on the front and none on the back. The front cliff sensor is really disappointing in the fact, that if Cozmo is moving forward at a pretty fast speed, he is going to fall off of a table. And, of course, going backwards, he will definitely fall of a table. He is much lighter than Vector, so that is why they can claim he is a tough little guy. But do not purposefully try this theory. You might be disappointed. As far as ever other thing, the people here have said just about everything there is to say.
- > He holds my attention like nothing else. Coding is a challenge I think I can grow into. And his charm— holy heck. Anki took the right direction by making not a robot cat, not a robot dog, but a robot “Cozmo” that is free to present pure personality on his own unique terms.
- > I was hoping for complete autonomy without the need of a phone always attached.
    >
    > Make him modular, but still as cute. Let me do things that are cool and more robust with it.
- > Anki did an awesome job with the Cozmo SDK because the code is well organized and easy to read/hack. I cannot recommend it enough. 

<div style="page-break-after: always;"></div>

Zalety:
- Bardzo dobre UX - wrażenie, że robot ma własną osobowość i jest zdolne do emocji 
- Przyjazny wygląd i zachowanie
- Dostarczone są dobre interfejsy do programowania robota
- Przyciąga uwagę i przynosi dużo rozrywki

Wady:
- Po pewnym czasie się nudzi jak poznało się już wszystkie umiejętności robota
- Wymaga ciągłego używania aplikacji mobilnej
- Problemy z łącznością po Wifi
- Problemu z baterią (krótki czas pracy, nie dokuje automatycznie)
- Delikatny

<div style="page-break-after: always;"></div>

### **[Buddy](https://buddytherobot.com/en/buddy-the-emotional-robot/)**

![](./docs/buddy.jpeg)

Robot o szerokim zastosowaniu: edukacja dzieci, wsparcie osób starszych, monitorowanie domu. Jest podobny do chińskiego Keeko: ma 60 cm wysokości, ekran dotykowy jako twarz, kółka. Jest robotem społecznym nastawionym na okazywanie emocji i komunikacje z ludźmi. Jest dość otwarty na rozbudowanie własnej funkcjonalności poprzez instalowanie nowych aplikacji, a dodatkowo ma dostępne SDK, co robi go interesującym wyborem dla developerów. Robot był możliwy do kupienia za ok. 2500zł na platformie crowdfundingowej w 2015 roku, jednak mimo powodzenia kampanii (616% zebranych funduszy), robot nadal nie został do nikogo dostarczony.

Opinię:
- Fragment recenzji na podstawie demonstracji producenta
    > The little white robot is cute enough, attracting a crowd of curious admirers and craning its neck around from the floor as people try to talk to it. But it hardly seems very useful. Buddy could perform a few tricks, like introducing itself, describing the local weather, and doing a little dance. But it kept mishearing commands, and it was entirely unable to strike up a meaningful conversation. As the executives repeatedly shouted “Dance, Buddy,” the robot’s face just rotated through a series of odd expressions, and it kept repeating the sad, confused phrase “I’m tired to talk.”
    >
    >After asking Buddy a few questions, I felt a little tired to talk, too. Perhaps the robot was just exhausted by the whole experience.
- Typowe opinie na stronie kampanii crowdfundingowej:
    > I would like my money back it’s been YEARS…

Zalety:
- Przyjazny wygląd
- Przyciąga uwagę

Wady:
- Nigdy nie trafił do klientów
- Mała funkcjonalność
- Deklarowana funkcjonalność nie działa dobrze (zła jakość rozpoznania mowy, twarzy itd.)

<div style="page-break-after: always;"></div>

## Persona - Nauczyciel
W analizie konkurencji nie można dobrze zauważyć persony rodzica. Konkurencja kierowała swój produkt głównie do nauczycieli lub wartości edukacyjne robotów były nie wielkie i jak już to głównie nastawione na urozmaicenie nauki programowania. Podczas gdy nasz robot nie jest tworzony z myślą o nauce programowania, to może być on używany przez nauczycieli jako dodatkowe narzędzie do nauki języków obcych. Z tego względu na podstawie opinii stworzyliśmy dodatkową personę.

![Kobieta w klasie ze zdjęciami](docs/anna.jpg)

[source]: # (https://cdn2.picryl.com/photo/2019/09/20/nicole-mcdonagh-a-hanscom-primary-school-teacher-1b082d-1024.jpg)

- Imię: Anna
- Wiek: 42
- Płeć: Kobieta
- Zawód: Nauczyciel
- Zarobki:  4 500
- Status: Mężatka
- Motto: "Talking is not teaching, listening is not learning"
### Biogram

Wrażliwa i empatyczna osoba. Kocha dzieci i komunikację z nimi. Uważa tradycyjne metody edukacji za niedoskonałe i nieustannie poszukuje nowych. Pracuje jako nauczycielka w podstawówce w Warszawie. Ma dwójkę dzieci (13 i 7 lat).

### Cele

Stara się być innowatorem w swoim zespole roboczym. Myśli o otwarciu w szkole własnego kółka dla dzieci z interaktywną nauką.

<div style="page-break-after: always;"></div>

### Zachowania
Spędza wiele czasu w szkole, chętnie prowadzi dodatkowe zajęcia pozalekcyjne. Przez resztę zespołu jest postrzegana jako osoba entuzjastyczna i zaangażowana, ale też nieudolna w swoich dziwnych próbach urozmaicenia nauki. Dużo czasu poświęca na wdrażanie eksperymentalnych metod nauczania. Lubi sprawdzać swoje pomysły na własnych dzieciach, przez co te mają dużo różnych zabawek i książek rozwijających. Często uczęszcza na konferencje edukacyjne. W wolnym czasie, albo spędza czas ze swoimi dziećmi, albo przegląda materiały popularnonaukowe. Szczególnie lubi filmy o eksperymentalnych metodach nauczania.

### Frustracje

Nie lubi uczyć dzieci z podręczników. Uważa że wtedy dzieci zniechęcają się do nauki. Jest niezadowolona, że w trakcie zajęć nauka jest ograniczana przez program nauczania.

## Wyzwanie
Robot do nauki języków, który przyciąga uwagę dziecka, wzbudza zainteresowanie, ma miły wygląd, tworzy przyjazną atmosferę oraz zachęca do nauki (zalety konkurencji), ale jest łatwo dostępny oraz w akceptowalnej cenie, oferuje regularnie nowe ćwiczenia, wykorzysta sprawdzone rozwiązania do rozpoznawania mowy oraz nie ma problemów technicznych, takich jak zbyt krótki czas pracy na baterii, przegrzewanie się, delikatna konstrukcja (wady konkurencji)
