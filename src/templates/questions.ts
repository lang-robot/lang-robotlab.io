const parser = new DOMParser();
export function keyword(key: string, header: string): Element {
  return parser.parseFromString(
    `<li class="question keyword">
      <span class="question__header">${header}</span>
      <span class="keyword__key">${key}</span>
    </li>`,
    'text/html',
  ).body.firstElementChild!;
}
export function charades(src: string): Element {
  return parser.parseFromString(
    `<li class="question charades">
      <span class="question__header">What is it?</span>
      <img class="charades__key" src="${src}" />
    </li>`,
    'text/html',
  ).body.firstElementChild!;
}
