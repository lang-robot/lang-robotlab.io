import Validator from "./Validator";
import { Question } from "../questions/Question";
import { showModal } from "../../utils/helpModal";
import speechToText from "../../utils/speechToText";
import getHelpPronounce from "../../utils/getHelpPronounce";
import say from "../../utils/says";
import sample from 'lodash.sample';

const whatIHeard = document.getElementById('what_i_heard');
const recButton = document.getElementById('rec-button');

export class Voice extends Validator {
  constructor(period) {
    super();
    this.scenes = ['quiz', 'menu']; // ... 'preferences', 'statistics', 'settings'];
    this.recording = true;

    this.promptsGiven = 0;

    this.speechToText = speechToText({
      words: this.scenes,
    });

    this.speechToText.applyNewCallback('show-what-heard', result => {
      whatIHeard.innerText = result.transcript.toLowerCase();
    });

  this.speechToText.applyNewCallback('help', result => {
      if (result.transcript.toLowerCase().includes('help')) {
        this.pauseRecording();
        showModal().then(() => this.startRecording());
      }
    });

    this.speechToText.start();

    recButton.addEventListener('click', () => {
      if (this.recording) {
        this.pauseRecording();
      } else {
        this.startRecording();
      }
    });
  }

  pauseRecording = () => {
    this.speechToText.pause();
    recButton.className = 'rec-button off';
    this.recording = false;
  };

  startRecording = () => {
    this.speechToText.start();
    recButton.className = 'rec-button on';
    this.recording = true;
  };

  voiceNavigator = callback => {

    const navigCallback = result => {
      const text = result.transcript.toLowerCase();
      if (text.includes('start')) {
        callback('quiz');
      }
      this.scenes.forEach(scene => {
        if (text.includes(scene)) {
          callback(scene);
        }
      });
    };

    this.speechToText.applyNewCallback('navigator', navigCallback);
  };

  validate = async Question => new Promise((res, rej) => {
    this.speechToText.updateWords(() => [...this.scenes, ...Question.answers]);

    const callbackId = `${Question.answers.join('')}validation`;

    const valCallback = (result, allResults) => {
      if (result.transcript.toLowerCase().includes('next')) {
        this.speechToText.removeCallback(callbackId);
        this.promptsGiven = 0;
        res();
      }

      const gotRightAnswer = Question.answers.find(ans => result.transcript.toLowerCase().replace(' ', '') === ans);

      if (gotRightAnswer) {
        this.speechToText.removeCallback(callbackId);
        this.promptsGiven = 0;
        this.pauseRecording();
        say(sample(['Good job!', 'Nice!', 'Right!', 'Great!', 'Is it so simple for you?', 'Today is your day', 'Excellent!', 'Brilliant!', 'You are genious!', 'That was smart!']))
          .then(this.startRecording);
        res();
        return;
      }

      const gotCloseAnswers = allResults.filter(({ transcript }) => Question.answers.some(ans => transcript.toLowerCase().includes(ans)));

      console.log(gotCloseAnswers);

      if (gotCloseAnswers.length) {
        const [theMostProbableFromClose] = gotCloseAnswers.sort((a, b) => b.confidence - a.confidence);
        const whatTriedToSay = Question.answers.find(ans => theMostProbableFromClose.transcript.includes(ans));

        this.pauseRecording();
        if (this.promptsGiven >= 3) {
          say('Okay, you were close.').then(this.startRecording);
          this.promptsGiven = 0;
          this.speechToText.removeCallback(callbackId);
          res();
        } else {
          setTimeout(() => {
            getHelpPronounce(whatTriedToSay).then(this.startRecording);
            this.promptsGiven += 1;
          }, 100);
        }

        return;
      }




      // Question.answers.forEach(ans => {
      //   if (result.transcript.toLowerCase().replace(' ', '') === ans) {
      //     this.speechToText.removeCallback(callbackId);
      //     this.promptsGiven = 0;
      //     this.pauseRecording();
      //     say(sample(['Good job!', 'Nice!', 'Right!', 'Great!', 'Is it so simple for you?', 'Today is your day', 'Excellent!', 'Brilliant!', 'You are genious!', 'That was smart!']))
      //       .then(this.startRecording);
      //     res();
      //   } else if (allResults.find(_result => _result.transcript.toLowerCase().includes(ans))) {
      //     this.pauseRecording();
      //     if (this.promptsGiven >= 3) {
      //       say('Okay, you were close.').then(this.startRecording);
      //       this.promptsGiven = 0;
      //       res();
      //     } else {
      //       setTimeout(() => {
      //         getHelpPronounce(ans).then(this.startRecording);
      //         this.promptsGiven += 1;
      //       }, 100);
      //     }
      //   }
      // });
    }

    this.speechToText.applyNewCallback(callbackId, valCallback);
  });
}
