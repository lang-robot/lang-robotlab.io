import Validator from "./Validator";
import { Question } from "../questions/Question";

let keyboardValidator;

export class Keyboard extends Validator {
  async validate(Question: Question) {
    let answers = Question.answers;
    let size = answers.reduce((prev, curr) => {
      return Math.max(prev, curr.length);
    }, 0);
    let history: string;
    let resolve: (value: unknown) => void;

    const promise = new Promise((_resolve, reject) => {
      resolve = _resolve;
      if (!document) {
        reject();
      }
    });

    let validator = (e: KeyboardEvent) => {
      history += e.key;
      if (history.length > size) {
        history.substring(1);
      }
      answers.forEach((answer) => {
        if (answer === history.substr(-answer.length)) {
          document.removeEventListener('keypress', validator);
          resolve(undefined);
          return;
        }
      });
    };
    keyboardValidator = validator;
    document.addEventListener('keypress', validator);
    return promise;
  }
}

export { keyboardValidator };
