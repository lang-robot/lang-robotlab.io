import { Question } from "../questions/Question";
export default abstract class Validator {
  abstract validate(Question: Question): Promise<unknown>
}