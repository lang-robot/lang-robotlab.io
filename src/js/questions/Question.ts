export abstract class Question {
  question;

  answers;

  header;

  context;

  constructor(
    question: string,
    answers: Array<string>,
    header: string,
    context?: Array<string>,
  ) {
    this.question = question;
    this.answers = answers;
    this.header = header;
    this.context = context ?? [question, ...answers];
  }

  abstract render(parent: HTMLElement): Element
}

export interface Questions {
  Questions: Question[];
  new(id: number): Questions
}
export abstract class Questions implements Questions {
  Questions: Question[] = [];
  [Symbol.iterator]() {
    var index = -1;
    var data = this.Questions;

    return {
      next: () => ({ value: data[++index], done: !(index in data) })
    };
  };
}