import { keyword } from '../../templates/questions';
import DB from '../../data/staticDB';
import { Question, Questions } from './Question';

export class Keyword extends Question implements Question {
  key;

  constructor(
    question: string,
    answers: Array<string>,
    header: string,
    key?: string,
  ) {
    super(question, answers, header);
    this.key = key ?? question;
  }

  render(parent: HTMLElement) {
    return parent?.appendChild(keyword(this.key, this.header));
  }
}

export class Antonyms extends Questions {
  Questions: Keyword[] = [];
  constructor(id: number) {
    super();
    if (DB.words[id].antonym.size > 0) {
      const key = DB.words[id].word;
      const question = `Opposite of ${key}`;
      const answers = Array.from(new Set([...DB.words[id].antonym, ...DB.words[id].antonym_extended])).map(wordId => DB.words[wordId].word);
      this.Questions.push(new Keyword(question, answers, "Antonym", key));
    }
  }
}
export class Synonyms extends Questions {
  Questions: Keyword[] = [];
  constructor(id: number) {
    super();
    if (DB.words[id].synonym.size > 0) {
      const key = DB.words[id].word;
      const question = `Other word for ${key}`;
      const answers = Array.from(new Set([...DB.words[id].synonym, ...DB.words[id].synonym_extended, ...DB.words[id].hyponym, ...DB.words[id].hyponym_extended, ...DB.words[id].hypernym, ...DB.words[id].hypernym_extended])).map(wordId => DB.words[wordId].word);
      this.Questions.push(new Keyword(question, answers, "Synonym", key));
    }
  }
}
export class Definitions extends Questions {
  Questions: Keyword[] = [];
  constructor(id: number) {
    super();
    DB.words[id].definition.forEach(definitionId => {
      const definition = DB.definitions[definitionId];
      const question = `What is ${definition}`;
      const answers = Array.from(definition.word).map(wordId => DB.words[wordId].word);
      this.Questions.push(new Keyword(question, answers, "Definition", definition.definition));
    });
  }
}
