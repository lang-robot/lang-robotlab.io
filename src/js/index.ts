// import { Charades } from './questions/Charades';
import { Definitions, Synonyms, Antonyms } from './questions/Keyword';
import { Keyboard } from './validator/keyboard';
// import { definitions, images } from '../data/data';
import DB from '../data/staticDB';
import { Voice } from './validator/voice';
import { speechToText } from '../utils/speechToText';
const questions = document.getElementById('questions');
const sceneButtons = document.getElementsByClassName('scene__button');
const { body } = document;

let validator = new Keyboard();
let voiceValidator = new Voice();


Array.from(sceneButtons)
  .filter((e): e is HTMLElement => e instanceof HTMLElement)
  .forEach((e) => {
    e.addEventListener('click', async () => {
      body.dataset.scene = e.dataset.scene;
    });
  });

voiceValidator.voiceNavigator(scene => {
  console.log(scene);
  body.dataset.scene = scene;
});

function shuffle(array: any[]) {
  var m = array.length, t, i;

  // While there remain elements to shuffle…
  while (m) {

    // Pick a remaining element…
    i = Math.floor(Math.random() * m--);

    // And swap it with the current element.
    t = array[m];
    array[m] = array[i];
    array[i] = t;
  }

  return array;
}
console.log(`Number of definitions:\t\t${DB.definitions.length}`)
console.log(`Words with definition:\t\t${Array.from(DB.words.keys()).filter(id => DB.words[id].definition.size > 0).length}`);
console.log(`Words with synonym:\t\t\t${Array.from(DB.words.keys()).filter(id => DB.words[id].synonym.size > 0).length}`);
console.log(`Words with antonym:\t\t\t${Array.from(DB.words.keys()).filter(id => DB.words[id].antonym.size > 0).length}`);
console.log(`Words with hypernym:\t\t${Array.from(DB.words.keys()).filter(id => DB.words[id].hypernym.size > 0).length}`);
console.log(`Words with hyponym:\t\t\t${Array.from(DB.words.keys()).filter(id => DB.words[id].hyponym.size > 0).length}`);
let qD = shuffle(Array.from(DB.words.keys()).filter(id => DB.words[id].definition.size > 0 && DB.words[id].synonym.size > 3));
let qS = shuffle(Array.from(DB.words.keys()).filter(id => DB.words[id].synonym.size > 3));
let qA = shuffle(Array.from(DB.words.keys()).filter(id => DB.words[id].antonym.size > 3));
let i = 0;
(async function () {
  if (questions instanceof HTMLElement) {
    // for (const word of DB.words.keys()
    while (true) {
      const D = new Definitions(qD[i]);
      const S = new Synonyms(qS[i]);
      const A = new Antonyms(qA[i]);
      let q = shuffle(D.Questions)[0];
      console.log(q.answers);
      let element = q.render(questions);
      await Promise.race([validator.validate(q), voiceValidator.validate(q)]);
      element.remove();

      q = S.Questions[0];
      console.log(q.answers);
      element = q.render(questions);
      await Promise.race([validator.validate(q), voiceValidator.validate(q)]);
      element.remove();

      q = A.Questions[0];
      console.log(q.answers);
      element = q.render(questions);
      await Promise.race([validator.validate(q), voiceValidator.validate(q)]);
      element.remove();
      i += 1;
    }
    // for (const index of images.keys()) {
    //   const Q2 = new Charades(index);
    //   console.log(Q2)
    //   for (let e of Q2) {
    //     let element = e.render(questions);
    //     await validator.validate(e);
    //     element.remove();
    //   }
    // }
  }

})();