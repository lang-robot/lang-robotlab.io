import sample from 'lodash.sample';

const getHelpPronounce = async text => new Promise(res => {
  const utterance = new SpeechSynthesisUtterance();

  const prompt = sample([
    `Try to say it like: ${text}`,
    `It's more like: ${text}`,
    `Repeat after me: ${text}`,
  ]);

  utterance.text = prompt;
  utterance.lang = 'en-US';

  utterance.addEventListener('end', res);
  utterance.addEventListener('error', err => console.error(err));

  document.getElementById('useful-button').click();
  speechSynthesis.speak(utterance);
});

export default getHelpPronounce;
