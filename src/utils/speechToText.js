import staticDB from "../data/staticDB";

const SpeechRecognition = window.SpeechRecognition || webkitSpeechRecognition;
const SpeechGrammarList = window.SpeechGrammarList || webkitSpeechGrammarList;
const SpeechRecognitionEvent = window.SpeechRecognitionEvent || webkitSpeechRecognitionEvent;


// recognition.onspeechend = function() {
//   recognition.stop();
// }

const speechToText = ({
  words,
}) => {
  const recognition = new SpeechRecognition();

  let _words = words;
  let callbacks = [];

  const setGrammar = () => {
    const grammar = '#JSGF V1.0; grammar words; public <word> = ' + _words.join(' | ') + ' ;';
    const speechRecognitionList = new SpeechGrammarList();

    speechRecognitionList.addFromString(grammar, 1);
    recognition.grammars = speechRecognitionList;
  };

  const applyNewCallback = (cbName, callback) => callbacks.push({ cbName, callback });

  const removeCallback = cbName => {
    callbacks = callbacks.filter(cb => cb.cbName !== cbName);
  };

  const updateWords = getNewWords => {
    _words = getNewWords(_words);
    setGrammar();
  };

  setGrammar(words);

  recognition.continuous = true;
  recognition.lang = 'en-US';
  recognition.interimResults = false;
  recognition.maxAlternatives = 10;

  recognition.onresult = event => {
    const results = [...event.results];

    const lastResults = [...results.pop()];

    const [mostProbable] = lastResults.sort((a, b) => b.confidence - a.confidence);

    for (let { callback } of callbacks) {
      callback(mostProbable, lastResults);
    }
  };

  const start = () => {
    recognition.start();
  };

  const pause = () => {
    recognition.stop();
  };

  return {
    start,
    pause,
    updateWords,
    applyNewCallback,
    removeCallback,
  };
};


export default speechToText;
