const say = async text => new Promise(res => {
  const utterance = new SpeechSynthesisUtterance();

  utterance.text = text;
  utterance.lang = 'en-US';

  utterance.addEventListener('end', res);
  utterance.addEventListener('error', err => console.error(err));

  document.getElementById('useful-button').click();
  speechSynthesis.speak(utterance);
});

export default say;
