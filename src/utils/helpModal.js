import { keyboardValidator } from "../js/validator/keyboard";
import getHelpPronounce from "./getHelpPronounce";

const parser = new DOMParser();

const showModal = () => new Promise(res => {
  document.removeEventListener('keypress', keyboardValidator);

  const quizScene = document.getElementById('quiz');

  quizScene.insertAdjacentHTML('beforeend', `
    <div id="help_modal" class="help_modal">
      <h1>HELP</h1>
      <input id="help_input" placeholder="Type there the word you want to say" />
      <button id="help_button" type="button">THEN PRESS HERE (OR ENTER, as you want, enter might be faster though)</button>
    </div>
  `);

  const enterListener = e => {
    if (e.key === 'Enter') {
      grabInputAndHelp();
    }
  };

  const hideModal = () => {
    const quizScene = document.getElementById('quiz');
  
    quizScene.removeChild(document.getElementById('help_modal'));
  
    removeEventListener('keypress', enterListener);
    document.addEventListener('keypress', keyboardValidator);
    res();
  };

  const grabInputAndHelp = async () => {
    const text = document.getElementById('help_input').value;
  
    await getHelpPronounce(text);
  
    hideModal();
  };

  document.getElementById('help_button').addEventListener('click', grabInputAndHelp);

  addEventListener('keypress', enterListener);
});

export {
  showModal, hideModal,
};
