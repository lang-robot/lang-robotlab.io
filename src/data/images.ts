export default [
  new URL(
    '../assets/cloud.jpg',
    import.meta.url,
  ),
  new URL(
    '../assets/cheese.jpg',
    import.meta.url,
  )
];