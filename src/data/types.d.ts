// declare module "*images.yaml" {
//   const images: Array<{
//     image: string,
//     answers: [string, ...string[]]
//   }>;
//   export default images;
// }
declare module "*words.json" {
  const words: Array<{
    word: string,
    definition: Set<number>,
    synonym: Set<number>,
    synonym_extended: Set<number>,
    hyponym: Set<number>,
    hyponym_extended: Set<number>,
    hypernym: Set<number>,
    hypernym_extended: Set<number>,
    antonym: Set<number>
    antonym_extended: Set<number>,
  }>;
  export default words;
}
declare module "*definitions.json" {
  const definitions: Array<{
    definition: string,
    word: Set<number>
  }>;
  export default definitions;
}
