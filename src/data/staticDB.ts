import { default as definitions } from './definitions.json';
import { default as words } from './words.json';
import fs from 'fs';
definitions.forEach(definition => {
  definition.word = new Set(definition.word);
});
words.forEach(word => {
  word.definition = new Set(word.definition);
  word.synonym = new Set(word.synonym);
  word.synonym_extended = new Set(word.synonym_extended);
  word.hyponym = new Set(word.hyponym);
  word.hyponym_extended = new Set(word.hyponym_extended);
  word.hypernym = new Set(word.hypernym);
  word.hypernym_extended = new Set(word.hypernym_extended);
  word.antonym = new Set(word.antonym);
  word.antonym_extended = new Set(word.antonym_extended);
});
const isNode =
  typeof process !== "undefined" &&
  process.versions != null &&
  process.versions.node != null;
type keysOfType<O, T> = { [K in keyof O]: O[K] extends T ? K : undefined }[keyof O] & string;
type setKeys<O, T> = keysOfType<O, Set<T>>;
function find<T>(data: Array<T>, key: keyof T, value: any) {
  return data.findIndex(data => data[key] === value);
};
function linkWords<K extends setKeys<typeof words[number], number>>(data: typeof words, key_1: K, id_1: number, key_2: K, id_2: number) {
  if (id_1 !== id_2) {
    link(data, key_1, id_1, data, key_2, id_2);
  }
};
function link<D extends { [k: string]: any }[], D1 extends D, D2 extends D, K1 extends setKeys<D1[number], number>, K2 extends setKeys<D2[number], number>>(data_1: D1, key_1: K1, id_1: number, data_2: D2, key_2: K2, id_2: number) {
  if (0 <= id_1 && id_1 <= data_1.length - 1 && 0 <= id_2 && id_2 <= data_2.length - 1) {
    data_1[id_1][key_1].add(id_2);
    data_2[id_2][key_2].add(id_1);
    // if (key_1 === 'synonym' || key_2 == 'synonym') {
    //   console.log(`link ${data_1[id_1].word}.${key_1} to ${data_2[id_2].word}`);
    //   console.log(`link ${data_2[id_2].word}.${key_2}: ${data_1[id_1].word}`);
    // }
  }
};
function addWords<K extends setKeys<typeof words[number], number>>(data: typeof words, key_1: K, ids_1: number[], key_2?: K, ids_2?: number[]) {
  if (ids_2 !== undefined) {
    ids_1.forEach(id_1 => {
      ids_2!.forEach(id_2 => linkWords(data, key_1, id_1, key_2 ?? key_1, id_2));
    });
  }
  else {
    const ids = Array.from(ids_1);
    ids.forEach((id_1, id) => {
      ids.slice(id + 1).forEach(id_2 => linkWords(data, key_1, id_1, key_1, id_2));
    })
  }
};

export class staticDB {
  definitions = definitions;
  words = words;
  findDefinition(definition: string) {
    return find(this.definitions, "definition", definition);
  };
  findDefinitions(definitions: string | string[]) {
    if (typeof definitions === "string") {
      return [this.findDefinition(definitions)];
    }
    return definitions.map(definition => {
      return this.findWord(definition);
    });
  };
  findWord(word: string) {
    return find(this.words, "word", word);
  };
  findWords(words: string | string[]) {
    if (typeof words === "string") {
      return [this.findWord(words)];
    }
    return words.map(word => {
      return this.findWord(word);
    });
  };
  linkDefinitionToWords(definitionId: number, wordId: number[]) {
    wordId.forEach(wordId => {
      link(this.definitions, "word", definitionId, this.words, "definition", wordId);
    });
  }
  addDefinition(wordsId: number[], definition: string) {
    let definitionId = this.findDefinition(definition);
    if (definitionId === -1) {
      definitionId = this.definitions.length;
      this.definitions.push({
        "definition": definition,
        "word": new Set()
      });
    }
    this.linkDefinitionToWords(definitionId, wordsId);
  }

  linkWords<K extends setKeys<typeof words[number], number>>(key_1: K, wordId1: number[], key_2?: K, wordId2?: number[]) {
    addWords(this.words, key_1, wordId1, key_2, wordId2);
  }
  linkSynonyms(word: number[], synonymsId: number[]) {
    this.linkWords("synonym", word, "synonym_extended", synonymsId);
    this.linkWords("synonym_extended", synonymsId);
  }
  linkAntonyms(word: number[], antonymsId: number[], synonymsId?: number[]) {
    this.linkWords("antonym", word, "antonym_extended", antonymsId);
    if (synonymsId !== undefined) {
      this.linkWords("antonym_extended", synonymsId, "antonym_extended", antonymsId);
    }
  }
  linkHypernyms(word: number[], hypernymId: number[], synonymsId?: number[]) {
    this.linkWords("hypernym", word, "hypernym_extended", hypernymId);
    if (synonymsId !== undefined) {
      this.linkWords("hypernym_extended", synonymsId, "hypernym_extended", hypernymId);
    }
  }
  linkHyponyms(word: number[], hyponymId: number[], synonymsId?: number[]) {
    this.linkWords("hyponym", word, "hyponym_extended", hyponymId);
    if (synonymsId !== undefined) {
      this.linkWords("hyponym_extended", synonymsId, "hyponym_extended", hyponymId);
    }
  }
  addToWords(word: string, synonyms?: string[], definitions?: string | string[], antonyms?: string | string[], hypernyms?: string | string[], hyponyms?: string | string[]) {
    let wordId = this.findWords([word]);
    if (typeof synonyms !== 'undefined') {
      this.linkSynonyms(wordId, this.findWords(synonyms));
    }
    let synonymsId = this.findWords(synonyms ?? []);
    if (typeof definitions !== 'undefined') {
      definitions = typeof definitions === "string" ? [definitions] : definitions;
      definitions.forEach(definition => {
        this.addDefinition([...wordId, ...synonymsId], definition);
      });
    }
    if (typeof antonyms !== 'undefined') {
      // if (antonyms.length) {
      //   console.log(words)
      //   console.log(antonyms)
      // }
      this.linkAntonyms(wordId, this.findWords(antonyms), synonymsId);
    }
    if (typeof hypernyms !== 'undefined') {
      this.linkHypernyms(wordId, this.findWords(hypernyms), synonymsId);
    }
    if (typeof hyponyms !== 'undefined') {
      this.linkHyponyms(wordId, this.findWords(hyponyms), synonymsId);
    }
  }
  definitionsToJSON() {
    return JSON.stringify(this.definitions, (_, value) => value instanceof Set ? Array.from(value) : value);
  }
  wordsToJSON() {
    return JSON.stringify(this.words, (_, value) => value instanceof Set ? Array.from(value) : value);
  }
  save(path: string) {
    if (!isNode) {
      console.log(path);
      console.log(this.definitionsToJSON());
      console.log(this.wordsToJSON());
    }
    else {
      // console.log(path);
      // console.log(this.definitionsToJSON());
      // console.log(this.wordsToJSON());
      fs.writeFile(`${path}words.json`, this.wordsToJSON(), err => {
        if (err) {
          console.error(err);
        }
        // file written successfully
      });
      fs.writeFile(`${path}definitions.json`, this.definitionsToJSON(), err => {
        if (err) {
          console.error(err);
        }
        // file written successfully
      });
    }
  }
}
export default new staticDB();