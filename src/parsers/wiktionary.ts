import fs from 'fs';
import { JSDOM } from 'jsdom';
import XmlSplit from 'xmlsplit';
import staticDB from '../data/staticDB';

function Section(data: string, group: string) {
  let start = data.search(new RegExp(`={2,5}${group}={2,5}`));
  if (start === -1) {
    return "";
  }
  data = data.substring(start);
  data = data.substring(data.search(group) + group.length);
  let level = data.search(/[^=]/);
  data = data.substring(level);
  data = data.substring(data.search(/^[^\r\n]/));
  let next = data.search(new RegExp(`\r?\n={${level}}[^=]*?={${level}}\r?\n`));
  if (next > -1) {
    data = data.substring(0, next);
  }
  return data;
}
function Words(data: string) {
  return data.split(/\r?\n/)?.map((line) => {
    return line.match(/(?:(ws\|)|(l\|en\|))(?<words>.*?)(?:\|+.*?)?(?:}})/)
      ?.groups?.words;
  }).filter((word): word is string => { return typeof word == "string" })
    .filter(word => staticDB.findWord(word) >= 0);
}
let Synonyms = (data: string) => Words(Section(data, "Synonyms"));
let Hyponyms = (data: string) => Words(Section(data, "Hyponyms"));
let Hypernyms = (data: string) => Words(Section(data, "Hypernyms"));
let Antonyms = (data: string) => Words(Section(data, "Antonyms"));
// let Instances = (data: string) => Words(Section(data, "Instances"));
// let Meronyms = (data: string) => Words(Section(data, "Meronyms"));
// let Holonyms = (data: string) => Words(Section(data, "Holonyms"));
// let Various = (data: string) => Words(Section(data, "Various"));
function removeTemplate(data: string, template: string, replace = '$1') {
  return data.replace(new RegExp(`(?:\\{\\{${template}\\|)(?:[^\\|\\}]*\\|)*([^\\|\\}]*)(?:\\}\\})`, 'g'), replace);
}
function filterRare(data: string[]) {
  let labels = [
    'dated',
    'obsolete',
    'archaic',
    '[^\\)]*slang',
    'historical',
    'informal',
    'rare',
    'entomology',
    'offensive',
    'humorous',
    'poetic'
  ];
  let isRare = new RegExp(`^\\s*\\((${labels.join('|')})\\)`);
  data = data.filter(definition => isRare.test(definition) === false);

  let templates = ['taxlink', 'alt', '[^\\|]*-', 'inflection', 'adj', 'pronunciation', 'present', 'misspelling', 'construed'];
  isRare = new RegExp(`\\{\\{(${templates.join('|')})[^\\}]*\\}\\}`);
  return data.filter(definition => isRare.test(definition) === false);
}
function clearText(data: string) {
  data = data.replace(/(?:\[\[)(?:[^\|\]]*\|)*([^\|\]]*)(?:\]\])/g, "$1");
  // data = data.replace(/(?:\&lt\;ref\&gt\;)(?:.*?)(?:\&lt\;\/ref\&gt\;)/g, "");
  data = data.replace(/(?:\&lt\;)(?:.*?)(?:\&gt\;)/g, "");
  data = data.replace("{{,}}", "");
  // data = data.replace(/(?:\&lt\;\!\-\-)(?:.*?)(?:\?\-\-\&gt\;)/g, "");
  // data = data.replace(/(?:\[)(?:[^\|\]]*\|)*(?:[^\|\]]*)(?:\])/g, "");
  let etymology_templates = ['derived', 'der', 'borrowed', 'bor',
    'learned borrowing', 'lbor', 'orthographic borrowing', 'obor',
    'inherited', 'inh', 'PIE root', 'affix', 'af', 'prefix', 'pre',
    'confix', 'con', 'suffix', 'suf', 'compound', 'com', 'blend',
    'clipping', 'short for', 'back-form', 'doublet', 'onomatopoeic',
    'onom', 'calque', 'cal', 'semantic loan', 'sl', 'named-after',
    'phono-semantic matching', 'psm', 'mention', 'm', 'cognate',
    'cog', 'noncognate', 'noncog', 'langname-mention', 'm\\+', 'rfe',
    'etystub', 'unknown', 'unk'];
  etymology_templates.forEach(template => data = removeTemplate(data, template));
  let link = ['Wikipedia', 'slim-wikipedia', 'Wikisource', 'Wikibooks', 'w', 'pedialite'];
  link.forEach(template => data = removeTemplate(data, template));
  data = removeTemplate(data, "R\\:[^\\|]*", "");
  data = removeTemplate(data, "l\\|en");
  data = removeTemplate(data, "synonym of");
  data = removeTemplate(data, "synonym");
  data = removeTemplate(data, "unsupported");
  data = removeTemplate(data, "cln", "");
  data = removeTemplate(data, "catlangname", "");
  data = removeTemplate(data, "senseid", "");
  data = removeTemplate(data, "defdate", "");
  data = removeTemplate(data, "abbreviation of", "");
  data = removeTemplate(data, "non-gloss definition", "");
  data = removeTemplate(data, "n-g", "");
  data = removeTemplate(data, "ngd", "");
  data = removeTemplate(data, "lb", "($1)");
  data = removeTemplate(data, "(q|qualifier)", "($1)");
  data = data.replace(/(?:'{3})([^\']+)(?:'{3})/g, '$1');
  data = data.replace(/(?:'{2})([^\']+)(?:'{2})/g, '$1');
  data = trim(data);
  // data = data.replace(/(?:\{\{l\|en\|)(?:[^\|\}]*\|)*([^\|\}]*)(?:\}\})/g, "$1");
  return data;
}
function trim(data: string) {
  return data.replace(/^\s/, '').replace(/\s$/, '').replace(/\.\s\./, '.');
}
function Definition(data: string, word: string) {
  let PoS = ["Adjective", "Adverb", "Ambiposition", "Article",
    "Circumposition", "Classifier", "Conjunction", "Contraction",
    "Counter", "Determiner", "Ideophone", "Interjection", "Noun",
    "Numeral", "Participle", "Particle", 'Postposition', "Preposition",
    "Pronoun", "Proper noun", "Verb"];
  let template = PoS.map(group => Section(data, group)).join('\r\n');
  let definitions: (string | undefined)[] = filterRare(template.split(/\r?\n/)
    .filter(line => /^# /.test(line)).map(line => clearText(line.substring(2)))
    .filter(line => /[^\s]/.test(line.replace(/\(.*?\)/g, ''))));

  let sense = data.match(/(?:ws sense\|)(?<definition>.*)(?:}})/)
    ?.groups?.definition;
  if (sense !== undefined) {
    definitions.push(clearText(sense));
  }
  return definitions
    .filter((word): word is string => { return typeof word == "string" })
    .filter(line => line?.search(new RegExp(word, 'i')) === -1)
    .filter(line => line.search("{{") === -1);
}

const path = process.argv[2] ?? "wiktionary.xml";
const output = process.argv[3] ?? "";
var xmlsplit = new XmlSplit(1, "page");
var inputStream = fs.createReadStream(path);
let i = 0;
const onFileContent = (content: string) => {
  i += 1;
  if (i % 3000 === 0) {
    console.log(`---------${inputStream.bytesRead}---------`);
    staticDB.save(output);
  }
  let xml!: JSDOM, word: string | undefined, template;
  try {
    xml = new JSDOM(content, { contentType: "application/xml" });
    word = xml.window.document.children[0].querySelector("title")?.innerHTML;
  }
  catch {
    return;
  }
  try {
    template = xml.window.document.children[0].querySelector("text")?.innerHTML;
  }
  catch {
    template = content.toString();
    template = template.substring(template.search(/(?:<text.*?)>.*/),
      template.search(/<\/text.*?>/));
    template = template.substring(template.search('>') + 1);
  }
  let thesaurus = word?.substring(0, word?.indexOf(':')) === "Thesaurus";
  word = word?.substring(word?.lastIndexOf(':') + 1);
  let id = -1;
  if (word !== undefined) {
    id = staticDB.findWord(word);
    if (id >= 0 && template !== undefined) {
      console.log(`${id}:\t ${word}`);
      template = Section(template, "English");
      // console.log(template);
      let synonyms = Synonyms(template);
      // console.log(Section(template, "Synonyms"));
      let antonyms = Antonyms(template);
      // console.log(Section(template, "Antonyms"));
      // if (antonyms.length) {
      //   console.log(`${id}:\t ${word}`);
      //   if (synonyms.length > 1) {
      //     console.log(`\t\tsynonyms: ${synonyms.join(', ')}`);
      //   }
      //   console.log(`\t\tantonyms: ${antonyms.join(', ')}`);
      // }
      let hyponyms = Hyponyms(template);
      // if (hyponyms.length) {
      //   console.log(`\t\thyponyms: ${hyponyms.join(', ')}`);
      // }
      let hypernyms = Hypernyms(template);
      // if (hypernyms.length) {
      //   console.log(`\t\thypernyms: ${hypernyms.join(', ')}`);
      // }
      let definition = Definition(template, word!);
      // if (definition.length) {
      //   console.log(`\t\tdefinition: \r\n\t\t\t${definition.join('\r\n\t\t\t')}`);
      // }
      if (thesaurus) {
        staticDB.addToWords(word, synonyms, definition, antonyms, hypernyms, hyponyms);
      }
      else {
        staticDB.addToWords(word, synonyms, undefined, antonyms, hypernyms, hyponyms);
      }
      // staticDB.addToWords(synonyms, undefined, antonyms, hypernyms, hyponyms);
      // else {
      //   console.log(template)
      // }
    }
  }
  // if (xml.window.document.querySelector("ns")?.innerHTML === "110") {
  // const page = xml.window.document.children[0].querySelector("text")?.innerHTML;
  // }
};

inputStream.pipe(xmlsplit).on('data', onFileContent);
